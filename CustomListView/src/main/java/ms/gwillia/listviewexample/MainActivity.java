package ms.gwillia.listviewexample;

import android.app.ListActivity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends ListActivity {
    private MessageDataSource datasource;
    ListView lv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        datasource = new MessageDataSource(this);
        datasource.open();

        List<Message> values = datasource.getAllMessages();

        // use the SimpleCursorAdapter to show the
        // elements in a ListView
        final ArrayAdapter<Message> adapter = new ArrayAdapter<Message>(this,
                android.R.layout.simple_list_item_1, values);
        setListAdapter(adapter);
        lv = getListView();
        lv.setLongClickable(true);
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int pos, long id) {
                Message comment = (Message) getListAdapter().getItem(pos);
                datasource.deleteMessage(comment);
                adapter.remove(comment);
                Toast.makeText(getApplicationContext(), "Deleted Message", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }

    // Will be called via the onClick attribute
    // of the buttons in main.xml
    public void onClick(View view) {
        @SuppressWarnings("unchecked")
        final ArrayAdapter<Message> adapter = (ArrayAdapter<Message>) getListAdapter();
        switch (view.getId()) {
            case R.id.add:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("WHAT DO YOU WANT FROM ME?");

                // Set up the input
                final EditText input = new EditText(this);
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String inputTxt = input.getText().toString();
                        Message comment = datasource.createMessage(inputTxt);
                        adapter.add(comment);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
                break;
            case R.id.delete:
                if (getListAdapter().getCount() > 0) {
                    Message comment = (Message) getListAdapter().getItem(0);
                    datasource.deleteMessage(comment);
                    adapter.remove(comment);
                }
                break;
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        datasource.open();
        super.onResume();
    }

    @Override
    protected void onPause() {
        datasource.close();
        super.onPause();
    }

} 
