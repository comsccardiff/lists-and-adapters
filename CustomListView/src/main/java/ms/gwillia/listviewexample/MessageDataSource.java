package ms.gwillia.listviewexample;

/**
 * Created by encima on 23/02/2016.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import ms.gwillia.listviewexample.utils.SqliteHelper;

public class MessageDataSource {

    // Database fields
    private SQLiteDatabase database;
    private SqliteHelper dbHelper;
    private String[] allColumns = { SqliteHelper.COLUMN_ID,
            SqliteHelper.COLUMN_MSG };

    public MessageDataSource(Context context) {
        dbHelper = new SqliteHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Message createMessage(String Message) {
        ContentValues values = new ContentValues();
        values.put(SqliteHelper.COLUMN_MSG, Message);
        long insertId = database.insert(SqliteHelper.TABLE_NAME, null,
                values);
        Cursor cursor = database.query(SqliteHelper.TABLE_NAME,
                allColumns, SqliteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Message newMessage = cursorToMessage(cursor);
        cursor.close();
        return newMessage;
    }

    public void deleteMessage(Message Message) {
        long id = Message.getId();
        System.out.println("Message deleted with id: " + id);
        database.delete(SqliteHelper.TABLE_NAME, SqliteHelper.COLUMN_ID
                + " = " + id, null);
    }

    public List<Message> getAllMessages() {
        List<Message> Messages = new ArrayList<Message>();

        Cursor cursor = database.query(SqliteHelper.TABLE_NAME,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Message Message = cursorToMessage(cursor);
            Messages.add(Message);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return Messages;
    }

    private Message cursorToMessage(Cursor cursor) {
        Message Message = new Message(cursor.getLong(0), cursor.getString(1));
//        Message.setId(cursor.getLong(0));
//        Message.setMsg(cursor.getString(1));
        return Message;
    }
} 

