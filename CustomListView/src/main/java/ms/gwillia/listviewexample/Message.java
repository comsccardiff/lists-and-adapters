package ms.gwillia.listviewexample;

/**
 * Created by encima on 23/02/2016.
 */
public class Message {
    private long id;
    private String msg;

    public Message(long id, String msg) {
        this.id = id;
        this.msg = msg;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    // Will be used by the ArrayAdapter in the ListView
    @Override
    public String toString() {
        return msg;
    }
}
