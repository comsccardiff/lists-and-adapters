package ms.gwillia.listviewexample;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

import static android.os.Build.VERSION.SDK_INT;
import static android.provider.Telephony.Sms.Intents.getMessagesFromIntent;

public class MessageReceiver extends BroadcastReceiver {
    public MessageReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // METHOD 1: SmsMessage
        SmsMessage[] messages = getMessagesFromIntent(intent);
        MessageDataSource msgDS = new MessageDataSource(context);
        msgDS.open();
        for(int i = 0; i < messages.length; i++) {
            msgDS.createMessage(messages[i].getMessageBody());
        }

        //Method 2: Protocol Data Unit
        Bundle intentExtras = intent.getExtras();
        // Get extras from the intent
        Object[] pdus = (Object[]) intentExtras.get("pdus");
        String format = intentExtras.get("extra").toString();
        for (int i = 0; i < pdus.length; i++) {
            SmsMessage sms;
            // Check Android version and use appropriate method
            if(SDK_INT >= 23) {
                sms = SmsMessage.createFromPdu((byte[]) pdus[i], format);
            }else{
                sms = SmsMessage.createFromPdu((byte[]) pdus[i]);
            }
            // Show message and save message to DB
            Toast.makeText(context, sms.getDisplayMessageBody(), Toast.LENGTH_SHORT).show();
            msgDS.createMessage(sms.getMessageBody());

        }
        // Abort the message going elsewhere if it contains this text
        if(messages[0].getMessageBody().contains("MSGDS:15675AU")) {
            abortBroadcast();
        }
        msgDS.close();
    }
}
