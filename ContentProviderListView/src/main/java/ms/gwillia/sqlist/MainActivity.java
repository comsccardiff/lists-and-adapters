package ms.gwillia.sqlist;

import android.Manifest;
import android.annotation.TargetApi;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import static android.os.Build.VERSION.SDK_INT;

public class MainActivity extends AppCompatActivity {

    private static final int FLAGS_REGISTER_CONTENT_OBSERVER = 2;
    //Android listview object
    ListView listViewPhoneBook;
    Cursor cursor;

    /** Called when the activity is first created. */
    @TargetApi(Build.VERSION_CODES.ECLAIR)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get the ListView Reference from xml file
        listViewPhoneBook=(ListView)findViewById(R.id.listPhoneBook);

        if(SDK_INT >= 23) {
            //check for permissions here, ask when version is Marshmallow or above
            //TODO: NEEDS to be improved with catches, rationales etc
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_CONTACTS}, 2);
        }else{
            //load the list, the manifest will handle the permissions
            setUpList();
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.i("SQLIST", "Result: " + grantResults[0]);
        //if permission has been approved, since we are only asking for one permission
        if(grantResults[0] == 0) {
            setUpList();
        } else {
            finish();
        }
    }

    @TargetApi(Build.VERSION_CODES.ECLAIR)
    public void setUpList() {
        //arrayColumns is the array which will contain all contacts name in your cursor, where the cursor will get the data from contacts database.
        //Here we are displaying name only from the contacts database
        String[] arrayColumns = new String[]{ContactsContract.Contacts.DISPLAY_NAME};
        //arrayViewID is the id of the view it will map to here textViewName only , you can add more Views
        int[] arrayViewID = new int[]{R.id.textViewName};

        //reference to the phone contacts database using Cursor and URI in android.
        cursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        //Creates a simple adapter that attached the contact.xml layout with the query for all contacts
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.contact, cursor, arrayColumns, arrayViewID, FLAGS_REGISTER_CONTENT_OBSERVER);
        listViewPhoneBook.setAdapter(adapter);

        // To handle the click on List View Item
        listViewPhoneBook.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // position parameter gives the index or position of ListView Item which is Clicked
                TextView tv = (TextView) view.findViewById(R.id.textViewName);
                String name = tv.getText().toString();
                Toast.makeText(getApplicationContext(), "Contact Selected: " + name, Toast.LENGTH_LONG).show();
                return true;
            }
        });
        listViewPhoneBook.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {
                // position parameter gives the index or position of ListView Item which is Clicked
                TextView tv=(TextView)v.findViewById(R.id.textViewName);
                String name=tv.getText().toString();
                Toast.makeText(getApplicationContext(), "Contact Selected: "+name, Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        //handles closing for GC
        cursor.close();
    }
}
